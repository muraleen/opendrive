# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/root/opendrive/src/ctlr.cxx" "/root/opendrive/build/armv7/CMakeFiles/openDrive.dir/src/ctlr.cxx.o"
  "/root/opendrive/src/drive.cxx" "/root/opendrive/build/armv7/CMakeFiles/openDrive.dir/src/drive.cxx.o"
  "/root/opendrive/src/main.cxx" "/root/opendrive/build/armv7/CMakeFiles/openDrive.dir/src/main.cxx.o"
  "/root/opendrive/src/pid.cxx" "/root/opendrive/build/armv7/CMakeFiles/openDrive.dir/src/pid.cxx.o"
  "/root/opendrive/src/route.cxx" "/root/opendrive/build/armv7/CMakeFiles/openDrive.dir/src/route.cxx.o"
  "/root/opendrive/src/strhelper.cxx" "/root/opendrive/build/armv7/CMakeFiles/openDrive.dir/src/strhelper.cxx.o"
  "/root/opendrive/src/usbgps.cxx" "/root/opendrive/build/armv7/CMakeFiles/openDrive.dir/src/usbgps.cxx.o"
  "/root/opendrive/src/vision.cxx" "/root/opendrive/build/armv7/CMakeFiles/openDrive.dir/src/vision.cxx.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/include/opencv"
  "../../include"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
