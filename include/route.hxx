#ifndef ROUTE_HXX
#define ROUTE_HXX

#include <vector>
#include <string>
#include <math.h>
#include <fstream>

using namespace std;

struct Wpt
{
	double lat;
	double lon;
};

class Route
{
	public:
		Route();
		void loadFromFile(string file);
		void appendWpt(Wpt wpt);
		void appendWpt(double lat, double lon);
		void insertWpt(int index, Wpt wpt);
		void insertWpt(int index, double lat, double lon);
		void deleteWpt(int index);
		Wpt getWpt(int index);
		Wpt makeWpt(double lat, double lon);
		double getHeadingToNextWpt();
		void update(double lat, double lon);
		void transitWpt();
		void dirToWpt(int index);
		double getDistanceToNextWpt();
	
	private:
		double wptTol;
		int currentWpt;
		Wpt position;
		vector<Wpt> wpts;
};

#endif
