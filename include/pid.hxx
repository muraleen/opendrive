#ifndef PID_H
#define PID_H

class PID
{
	public:
		PID(double _Kp, double _Ki, double _Kd);
		PID(double _Kp, double _Ki, double _Kd, double _iLimit);
		double run(double error, double dt);
	
	private:
		double Kp;
		double Ki;
		double Kd;
		double iLimit;
		double _error;
		double errorInt;
};

#endif
