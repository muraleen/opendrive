#ifndef VISION_H
#define VISION_H

#include <iostream>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

using namespace std;
using namespace cv;

class CV
{
	public:
		CV(int id);
		void setHSVThreshold(int _Hl, int _Hh, int _Sl, int _Sh, int _Vl, int _Vh);
		void process();
		bool isReady();
		double getX();
		double getY();
	
	private:
		VideoCapture cap;
	
		double posX;
		double posY;
		
		int Hl;
		int Hh;
		int Sl;
		int Sh;
		int Vl;
		int Vh;
		
		int width;
		int height;
};

#endif
