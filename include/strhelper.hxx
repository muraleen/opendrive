#ifndef STRHELPER_H
#define STRHELPER_H

#include <vector>
#include <string>

using namespace std;

class StrHelper
{
	public:
		static vector<string> splitStr(string input, string delimiter);
		static bool isNumber(string str);
};

#endif
