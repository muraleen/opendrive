#ifndef DRIVE_H
#define DRIVE_H

using namespace std;

class Drive
{
	public:
		static void init();
		static void write(int byte);
		static void setSpeed(int speed);
		static void setSteering(int steer);
		static void stop();
};

#endif
