#ifndef USBGPS_H
#define USBGPS_H

#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

class UsbGPS
{
	public:
		UsbGPS(char* _port);
		~UsbGPS();
		bool process();
		int getUTC();
		double getLat();
		double getLon();
		double getAlt();
		double getSpd();
		double getTrk();
	private:
		char* port;
		ifstream in;
		int utc;
		double latitude; // deg
		double longitude; // deg
		double altitude; // m
		double groundSpeed; // m/s
		double trackAngle; // deg
		double NMEA2deg(string NMEAstr);
};

#endif
