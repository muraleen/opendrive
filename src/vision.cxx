#include "vision.hxx"

CV::CV(int id)
{
	cap.open(id);
	
	if (!cap.isOpened()) {
		cerr << "Could not open camera" << endl;
		exit(-1);
	}

	// cap.set(CV_CAP_PROP_FRAME_WIDTH, 640);
	// cap.set(CV_CAP_PROP_FRAME_HEIGHT, 360);
	
	// Default HSV thresholds set for RED in moderate lighting conditions
	Hl = 146;
	Hh = 179;
	Sl = 165;
	Sh = 255;
	Vl = 50;
	Vh = 255;
	
	Mat imgTmp;
	cap.read(imgTmp);
	
	width = imgTmp.cols;
	height = imgTmp.rows;
	
	posX = 100;
	posY = 100;
}

void CV::setHSVThreshold(int _Hl, int _Hh, int _Sl, int _Sh, int _Vl, int _Vh)
{
	this->Hl = _Hl;
	this->Hh = _Hh;
	this->Sl = _Sl;
	this->Sh = _Sh;
	this->Vl = _Vl;
	this->Vh = _Vh;
}

void CV::process()
{
	Mat imgOrig;
	
	bool bSuccess = cap.read(imgOrig);
	
	if (!bSuccess) {
		cerr << "Cannot read from video stream" << endl;
	} else {
	
		Mat imgHSV;
	
		cvtColor(imgOrig, imgHSV, COLOR_BGR2HSV);
	
		Mat imgThresh;
	
		inRange(imgHSV, Scalar(Hl, Sl, Vl), Scalar(Hh, Sh, Vh), imgThresh);
	
		erode(imgThresh, imgThresh, getStructuringElement(MORPH_ELLIPSE, Size(5,5)));
		dilate(imgThresh, imgThresh, getStructuringElement(MORPH_ELLIPSE, Size(5,5)));
	
		dilate(imgThresh, imgThresh, getStructuringElement(MORPH_ELLIPSE, Size(5,5)));
		erode(imgThresh, imgThresh, getStructuringElement(MORPH_ELLIPSE, Size(5,5)));
	
		Moments oMoments = moments(imgThresh);
	
		double dM01 = oMoments.m01;
		double dM10 = oMoments.m10;
		double dArea = oMoments.m00;
	
		if (dArea > 5000) {
			posX = dM10 / dArea;
			posY = dM01 / dArea;
		} else {
			posX = -1;
			posY = -1;
		}
	}
}

bool CV::isReady()
{
	if (posX >= 0 && posY >= 0) return true;
	else return false;
}

double CV::getX()
{
	return 2*posX/(double)width - 1;
}

double CV::getY()
{
	return 1 - 2*posY/(double)height;
}
