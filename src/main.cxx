#include "includes.hxx"

using namespace std;

int main(int argc, const char* argv[]) {

	/* UsbGPS *gps = new UsbGPS("/dev/ttyACM0");
	Route *route = new Route();
	
	// route->loatFromFile("waypoints.rte");
	
	route->appendWpt(34.557047, -112.388530);
	
	while (true) {
		gps->process();
	
		route->update(gps->getLat(), gps->getLon());
	
		float targetHdg = route->getHeadingToNextWpt();
		float dist2Wpt = route->getDistanceToNextWpt();
		
		cout << "Lat: " << gps->getLat() << "\t\tLon: " << gps->getLon() << endl; 
	
		cout << "Target Heading: " << targetHdg << " deg" << endl;
		cout << "Distance to Wpt: " << dist2Wpt << " m" << endl << endl;
		
		sleep(1);
	} */
	
	CV *vis = new CV(0);
	PID *steeringCtl = new PID(100, 40, 10, 60);
	Drive::init();
	
	auto start = chrono::high_resolution_clock::now();
	auto curr = chrono::high_resolution_clock::now();
	auto dt_chrono = curr - start;
	long long dt_us;
	double dt_s;
	
	while (true) {

		// Used openCV to find target's relative X position
		vis->process();
	
		// Compute time elapsed between loop frames
		curr = chrono::high_resolution_clock::now();
		dt_chrono = curr-start;
		dt_us = chrono::duration_cast<chrono::microseconds>(dt_chrono).count();
		dt_s = ((double) dt_us)/1000000;
		start = curr;
		
		// Control Drive System		
		if (vis->isReady()) {
			double steer = saturate(steeringCtl->run(vis->getX(), dt_s), -100, 100);
			cout << vis->getX() << "\t\t" << steer << endl;
			Drive::setSteering(steer);
		}
	}

	return 1;
}
