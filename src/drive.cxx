#include "drive.hxx"

#include <stdlib.h>
#include <sstream>
#include <string>

void Drive::init()
{
	system("stty -F /dev/ttyS2 38400");
}

void Drive::write(int byte)
{
	stringstream hexVal;
	hexVal << hex << byte;
	
	string writeString = "echo -n -e '\\x" + hexVal.str() + "' > /dev/ttyS2";
	const char* c = writeString.c_str();
	system(c);
}

void Drive::setSpeed(int speed) // Between -100% and 100%
{
	write((int) (64 + (speed*63)/100));
}

void Drive::setSteering(int steer) // Between -100% and 100%
{
	write((int) (192 + (steer*63)/100));
}

void Drive::stop()
{
	write(0);
}
