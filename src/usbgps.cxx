#include "usbgps.hxx"
#include "strhelper.hxx"

UsbGPS::UsbGPS(char* _port)
{
	this->port = _port;
	this->in.open(_port);
	this->utc = 0;
	this->latitude = 0;
	this->longitude = 0;
	this->altitude = 0;
	this->groundSpeed = 0;
	this->trackAngle = 0;
}

UsbGPS::~UsbGPS()
{
	in.close();
}

int UsbGPS::getUTC()
{
	return this->utc;
}

double UsbGPS::getLat()
{
	return this->latitude;
}

double UsbGPS::getLon()
{
	return this->longitude;
}

double UsbGPS::getAlt()
{
	return this->altitude;
}

double UsbGPS::getSpd()
{
	return this->groundSpeed;
}

double UsbGPS::getTrk()
{
	return this->trackAngle;
}

bool UsbGPS::process()
{
	bool valid = true; // Temporary hack

	if(in.is_open()) {
		string line;
		bool dataSetReceived = false;
		while(!dataSetReceived) {
			getline(in, line);
			
			vector<string> NMEAdata = StrHelper::splitStr(line, ",");

			if(NMEAdata.size() > 0) {
				if(NMEAdata.at(0).compare("$GPRMC") == 0) { // Recommended min. data for GPS
					if(NMEAdata.at(2).compare("A") == 0) { // ACTIVE
						this->utc = stoi(NMEAdata.at(1));
						if(NMEAdata.at(4).compare("N") == 0) {
							this->latitude = NMEA2deg(NMEAdata.at(3));
						} else {
							this->latitude = -NMEA2deg(NMEAdata.at(3));
						}
						if(NMEAdata.at(6).compare("E") == 0) {
							this->longitude = NMEA2deg(NMEAdata.at(5));
						} else {
							this->longitude = -NMEA2deg(NMEAdata.at(5));
						}
						if(StrHelper::isNumber(NMEAdata.at(7))) {
							this->groundSpeed = stod(NMEAdata.at(7))*0.5144;
						}
						if(StrHelper::isNumber(NMEAdata.at(8))) {
							if(stod(NMEAdata.at(8)) < 360) {
								this->trackAngle = stod(NMEAdata.at(8));
							}
						}
						valid = true;
					}
				} else if(NMEAdata.at(0).compare("$GPGGA") == 0) { // Fix Information
					if(stoi(NMEAdata.at(6)) > 0) {
						this->utc = stoi(NMEAdata.at(1));
						if(NMEAdata.at(3).compare("N") == 0) {
							this->latitude = NMEA2deg(NMEAdata.at(2));
						} else {
							this->latitude = -NMEA2deg(NMEAdata.at(2));
						}
						if(NMEAdata.at(5).compare("E") == 0) {
							this->longitude = NMEA2deg(NMEAdata.at(4));
						} else {
							this->longitude = -NMEA2deg(NMEAdata.at(4));
						}
						if(StrHelper::isNumber(NMEAdata.at(9))) {
							this->altitude = stod(NMEAdata.at(9));
						}
						valid = true;
					}
				} else if(NMEAdata.at(0).compare("$GPGLL") == 0) { // Last of Set
					dataSetReceived = true;
				}
			}
		}
	}
	
	return valid;
}

double UsbGPS::NMEA2deg(string NMEAstr)
{
	double deg_min = stod(NMEAstr);
	
	int deg_i = deg_min / 100;
	double min = deg_min - (100.0*((double) deg_i));
	
	double deg = ((double) deg_i) + (min*5.0/300.0);
	
	return deg;
}
