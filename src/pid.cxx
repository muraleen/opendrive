#include "pid.hxx"

PID::PID(double _Kp, double _Ki, double _Kd)
{
	Kp = _Kp;
	Ki = _Ki;
	Kd = _Kd;
	iLimit = 0;
}

PID::PID(double _Kp, double _Ki, double _Kd, double _iLimit)
{
	Kp = _Kp;
	Ki = _Ki;
	Kd = _Kd;
	iLimit = _iLimit;
}

double PID::run(double error, double dt)
{
	if (errorInt + error*dt > iLimit) {
		errorInt = iLimit;
	} else if (errorInt + error*dt < -iLimit) {
		errorInt = -iLimit;
	} else {
		errorInt += error*dt;
	}
	
	double output;
	
	output = Kp*error;
	output += Ki*errorInt;
	output += Kd*((error - _error)/dt);
	
	_error = error;
	
	return output;
}

