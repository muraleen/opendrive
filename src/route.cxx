#include "route.hxx"
#include "strhelper.hxx"

#define PI 3.14159265
#define NM2M 1852

Route::Route()
{
	// Constructor
	currentWpt = 0;
	wptTol = 1;
}

void Route::loadFromFile(string file)
{
	string line;
	ifstream dataFile (file);
	if (dataFile.is_open()) {
		while (getline(dataFile, line)) {
			vector<string> wptStr = StrHelper::splitStr(line, ",");
			appendWpt(stod(wptStr.at(0)), stod(wptStr.at(1)));
		}
	}
}

Wpt Route::getWpt(int index)
{
	return wpts.at(index);
}

Wpt Route::makeWpt(double lat, double lon)
{
	Wpt wpt;
	wpt.lat = lat;
	wpt.lon = lon;
	
	return wpt;
}

void Route::appendWpt(Wpt wpt)
{
	wpts.push_back(wpt);
}

void Route::appendWpt(double lat, double lon)
{
	appendWpt(makeWpt(lat, lon));
}

void Route::insertWpt(int index, Wpt wpt)
{
	wpts.resize(wpts.size() + 1);
	for (int i=wpts.size()-2; i>=index; i--) {
		wpts.at(i+1) = wpts.at(i);
	}
	wpts.at(index) = wpt;
}

void Route::insertWpt(int index, double lat, double lon)
{
	insertWpt(index, makeWpt(lat, lon));
}

void Route::deleteWpt(int index)
{
	for (int i=index; i<wpts.size(); i++) {
		wpts.at(i) = wpts.at(i+1);
	}
	wpts.resize(wpts.size()-1);
}

double Route::getHeadingToNextWpt()
{
	Wpt target = wpts.at(currentWpt);
	
	return (double) 90 - (atan2(target.lat - position.lat, target.lon - position.lon)*(180.0/PI));
}

double Route::getDistanceToNextWpt()
{
	Wpt target = wpts.at(currentWpt);

	double deltaY = (target.lat - position.lat)*60*NM2M;
	double deltaX = (target.lon - position.lon)*60*NM2M;
	
	return sqrt(pow(deltaX,2) + pow(deltaY,2));
}

void Route::update(double lat, double lon)
{
	position.lat = lat;
	position.lon = lon;
	
	if (getDistanceToNextWpt() < wptTol) transitWpt();
}

void Route::transitWpt()
{
	currentWpt++;
}

void Route::dirToWpt(int index)
{
	currentWpt = index;
}
