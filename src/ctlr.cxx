#include "ctlr.hxx"

double saturate(double val, double lo, double hi)
{
	if (val > hi) {
		return hi;
	} else if (val < lo) {
		return lo;
	} else {
		return val;
	}
}
